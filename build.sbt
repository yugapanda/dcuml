

lazy val root = (project in file(".")).settings(
  name := "DCUML",
  organization := "com.hynfias",
  version := "0.1",
  scalaVersion := "2.12.8",
  libraryDependencies ++= List(
    "io.circe" % "circe-yaml_2.12" % "0.9.0",
    "io.circe" %% "circe-generic" % "0.11.0",
    "io.circe" %% "circe-parser" % "0.11.0"
  ),
  mainClass := Some("com.hynfias.dcuml.Main"),
  mainClass in assembly := Some("com.hynfias.dcuml.Main"),
  assemblyJarName in assembly := "dcuml.jar",
)
