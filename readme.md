# DCUML  

## What's this  

This is a converter for docker-compose.yml to plantUML.  

[download](https://bitbucket.org/yugapanda/dcuml/downloads/dcuml.jar)  

ex)  

```  
java -jar dcuml.jar -in /path/to/docker-compose.yml -out /path/to/UMLFile.pu
```  

input docker-compose.yml  
```yml
version: '2'
services:
  mongodb:
    image: mongo:3.4
    volumes:
      - ./data/mongo:/data/db
    restart: always
  registry:
    image: consul:1.0.6
    volumes:
      - ./data/consul:/consul/data
    ports:
        - "8500:8500"
    command: "agent -server -bootstrap-expect=1 -ui -client 0.0.0.0"
    environment:
        - 'CONSUL_LOCAL_CONFIG={"leave_on_terminate": true}'
    restart: always
  wss:
    image: ww/sss:1.2.0
    depends_on:
      - mongodb
    environment:
      - RP_PROFILES=docker
      - RP_SESSION_LIVE=86400
    restart: always
  gateway:
    image: traefik:1.6.6
    ports:
      - "8080:8080"
    command:
      - --consulcatalog.endpoint=registry:8500
      - --defaultEntryPoints=http
      - --entryPoints=Name:http Address::8080
      - --web
      - --web.address=:8081
    restart: always
  index:
    image: ww/sss:4.2.0
    depends_on:
       - registry
       - gateway
    restart: always

  api:
    image: reportportal/service-api:4.3.4
    depends_on:
      - mongodb
    environment:
      - RP_PROFILES=docker
      - JAVA_OPTS=-Xmx1g -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp
    restart: always

  ux:
    image: ww/ss-ui:4.3.6
    environment:
      - RP_SERVER.PORT=8080
      - RP_CONSUL.TAGS=urlprefix-/ui opts strip=/ui
      - RP_CONSUL.ADDRESS=registry:8500
    restart: always
  analyzer:
    image: ww/ss-analyzer:4.3.0
    depends_on:
       - registry
       - gateway
       - elasticsearch
    links:
       - ux
    restart: always

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:6.1.1
    restart: always
    volumes:
        - ./data/elasticsearch:/usr/share/elasticsearch/data
    environment:
      - bootstrap.memory_lock=true

```  

output docker-compose.pu  
```puml
@startuml docker compose 
object mongodb
object registry
object wss
object gateway
object index
object api
object ux
object analyzer
object elasticsearch
mongodb : volumes:\n ./data/mongo:/data/db\n
mongodb : image:\n mongo:3.4\n
registry : ports:\n 8500:8500 \n
registry : volumes:\n ./data/consul:/consul/data\n
registry : image:\n consul:1.0.6\n
registry : environment:\n CONSUL_LOCAL_CONFIG={"leave_on_terminate": true}\n
wss : image:\n ww/sss:1.2.0\n
wss : environment:\n RP_PROFILES=docker\nRP_SESSION_LIVE=86400\n
wss --> mongodb : depends_on
gateway : ports:\n 8080:8080 \n
gateway : image:\n traefik:1.6.6\n
index : image:\n ww/sss:4.2.0\n
index --> registry : depends_on
index --> gateway : depends_on
api : image:\n reportportal/service-api:4.3.4\n
api : environment:\n RP_PROFILES=docker\nJAVA_OPTS=-Xmx1g -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp\n
api --> mongodb : depends_on
ux : image:\n ww/ss-ui:4.3.6\n
ux : environment:\n RP_SERVER.PORT=8080\nRP_CONSUL.TAGS=urlprefix-/ui opts strip=/ui\nRP_CONSUL.ADDRESS=registry:8500\n
analyzer : image:\n ww/ss-analyzer:4.3.0\n
analyzer --> registry : depends_on
analyzer --> gateway : depends_on
analyzer --> elasticsearch : depends_on
analyzer --|> ui : links
elasticsearch : volumes:\n ./data/elasticsearch:/usr/share/elasticsearch/data\n
elasticsearch : image:\n docker.elastic.co/elasticsearch/elasticsearch-oss:6.1.1\n
elasticsearch : environment:\n bootstrap.memory_lock=true\n
@enduml
```  

![image](https://bitbucket.org/yugapanda/dcuml/downloads/dcuml.png)  
