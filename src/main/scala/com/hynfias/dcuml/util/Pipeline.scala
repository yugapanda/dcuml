package com.hynfias.dcuml.util
import scala.language.implicitConversions
import scala.language.reflectiveCalls

import scala.language.implicitConversions
import scala.language.reflectiveCalls
object AllExt {
  implicit def Pipeline[T](x: T) = new {
    def |>[S](f: T => S): S = f(x)
  }
}