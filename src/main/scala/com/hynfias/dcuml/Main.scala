package com.hynfias.dcuml

import com.hynfias.dcuml.model.Command
import com.hynfias.dcuml.service.YamlParser._
import com.hynfias.dcuml.service.PlantUML._
import com.hynfias.dcuml.util.AllExt._

import scala.io.Source
import java.io.PrintWriter

import com.hynfias.dcuml.util._


object Main {
  def main(args: Array[String]): Unit = {

    val command = Command.make(args)

    command.isOK match {
      case Right(x) => make(x)
      case Left(x) => println(x)
    }

  }

  def make(command: Command): Unit = {

    Using(resource = Source.fromFile(command.inPath.get, "UTF-8")) { in =>
      Using(resource = new PrintWriter(command.outPath.get)) { out =>
        val uml = parse(in.getLines.toList.mkString("\n")) |> makeUml
        out.write(uml.mkString("\n"))
      }
    }
    println("ok")
  }

}
