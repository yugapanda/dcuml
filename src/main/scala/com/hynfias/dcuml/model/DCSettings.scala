package com.hynfias.dcuml.model

case class Service(build: String, container_name: String, volumes: List[String], ports: List[String])
case class DCSettings(version: String, services: List[Service])

