package com.hynfias.dcuml.model

object Command {
  def make(args: Array[String]): Command = {
    def loop(args: List[String], acc: Command): Command = args match {
      case "-in" :: x :: xs => loop(xs, Command(Some(x), acc.outPath))
      case "-out" :: x :: xs => loop(xs, Command(acc.inPath, Some(x)))
      case _ => acc
    }

    loop(args.toList, Command(None, None))

  }
}

case class Command(inPath: Option[String], outPath: Option[String]) {
  def isOK: Either[String, Command] = this match {
    case Command(Some(_), Some(_)) => Right(this)
    case Command(Some(_), None) => Left("Need output path")
    case Command(None, Some(_)) => Left("Need input path")
    case _ => Left("Need input and output path")
  }
}
