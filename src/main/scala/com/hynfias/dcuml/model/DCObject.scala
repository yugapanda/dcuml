package com.hynfias.dcuml.model

import com.hynfias.dcuml.util.AllExt.Pipeline

case class DCObject(
                     serviceName: Option[String],
                     containerName: Option[String],
                     ports: List[Option[String]],
                     volumes: List[Option[String]],
                     dependsOn: List[Option[String]],
                     build: Option[String],
                     image: Option[String],
                     environment: List[Option[String]],
                     links: List[Option[String]]
                   ) {

  private def getListParams(list: List[Option[String]]): Option[String] = list.collect {
    case x if x.isDefined => x.get
  }.mkString("\\n") match {
    case x if x == "" => None
    case x => Some(x)
  }

  private def getListParamsForArrow(list: List[Option[String]],
                                    name: String,
                                    arrow: String,
                                    words: String): List[Option[String]] = list
    .map(_.map(x => s"$name $arrow ${x.replace("-", "_")} : $words"))

  def makeObject: List[String] = {

    val name = serviceName match {
      case Some(x) => x.replace("-", "_")
      case _ => ""
    }

    val umlList = List(
      Some(s"object $name"),
      containerName.map(x => s"""$name : container name: \\n $x \\n"""),
      getListParams(ports).map(x => s"""$name : ports:\\n $x \\n"""),
      getListParams(volumes).map(x => s"""$name : volumes:\\n $x\\n"""),
      build.map(x => s"""$name : build:\\n $x\\n"""),
      image.map(x => s"""$name : image:\\n $x\\n"""),
      getListParams(environment).map(x => s"""$name : environment:\\n $x\\n""")
    ) ++
      getListParamsForArrow(dependsOn, name, "-->", "depends_on") ++
      getListParamsForArrow(links, name, "--|>", "links")

    umlList.collect {
      case x if x.isDefined => x.get
    }

  }

}
