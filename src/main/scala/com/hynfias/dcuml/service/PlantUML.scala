package com.hynfias.dcuml.service

import com.hynfias.dcuml.model.DCObject
import com.hynfias.dcuml.util.AllExt.Pipeline

object PlantUML {

  def makeUml(dCObjects: List[DCObject]): List[String] = {
    addStartEnd("docker compose ", dCObjects.flatMap(_.makeObject) |> sortObjects)
  }


  private def sortObjects(list: List[String]): List[String] = {
    val objects = list.filter(_.contains("object"))
    val other = list.filter(!_.contains("object"))
    objects ++ other
  }

  private def addStartEnd(name: String, uml: List[String]): List[String] = {
    val start = (s"@startuml $name" :: uml).reverse
    ("@enduml" :: start).reverse
  }


}
