package com.hynfias.dcuml.service

import io.circe._

import io.circe.yaml.parser
import com.hynfias.dcuml.model._
import com.hynfias.dcuml.util.AllExt._

object YamlParser {

  case class NameAndService(name: String, service: Json)

  def parse(yaml: String): List[DCObject] = {

    val json: Either[ParsingFailure, Json] = parser.parse(yaml)
    val keys: List[String] = getKeys(json)
    val builds: List[Json] = getServices(keys, json)

    keys
      .zip(builds)
      .map(x => NameAndService(x._1, x._2)) |> makeDCObject

  }

  private def getKeys(json: Either[ParsingFailure, Json]): List[String] =
    json.map(_ \\ "services")
    .map(_
      .map(_
        .hcursor.keys)) match {
    case Right(List(Some(xs))) => xs.toList
    case _ => Nil
  }

  private def getServices(keys: List[String], json: Either[ParsingFailure, Json]): List[Json] =
    keys.map(key => json.map(j => j \\ key))
    .foldLeft(List[Json]())((acc, n) => if (n.isRight) n.right.get ++ acc else acc)
    .reverse


  def makeDCObject(nameAndServices: List[NameAndService]): List[DCObject] = nameAndServices.map(x => {
    DCObject(
      serviceName = Some(x.name),
      containerName = getStringParam("container_name", x),
      ports = getArrayParams("ports", x),
      volumes = getArrayParams("volumes", x),
      dependsOn = getArrayParams("depends_on", x),
      build = getStringParam("build", x),
      image = getStringParam("image", x),
      environment = getArrayParams("environment", x),
      links = getArrayParams("links", x)
    )
  })

  private def getStringParam(paramName: String, nameAndService: NameAndService): Option[String] =
    (nameAndService.service \\ paramName).headOption.flatMap(_.asString)

  private def getArrayParams(
                              paramName: String,
                              nameAndService: NameAndService
                            ): List[Option[String]] =
    (nameAndService.service \\ paramName)
    .headOption.flatMap(_.asArray.map(_.map(_.asString).toList)) match {
    case Some(xs) => xs
    case None => Nil
  }


}
